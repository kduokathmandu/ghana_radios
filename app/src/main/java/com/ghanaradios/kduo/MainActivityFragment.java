package com.ghanaradios.kduo;

import android.app.Activity;
import android.app.Application;
import android.app.Fragment;
import android.app.SearchManager;
import android.app.TimePickerDialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.net.Uri;
import android.os.Bundle;
import android.os.Parcelable;
import android.preference.PreferenceManager;
import android.support.annotation.NonNull;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v4.content.LocalBroadcastManager;
import android.support.v7.app.ActionBar;
import android.support.v7.widget.CardView;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.SearchView;
import android.text.TextUtils;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.TimePicker;
import android.widget.Toast;

import com.ghanaradios.kduo.app.AppStation;
import com.ghanaradios.kduo.app.CustomStation;
import com.ghanaradios.kduo.app.MyRadio;
import com.ghanaradios.kduo.chat.login.LoginActivity;
import com.ghanaradios.kduo.core.Station;
import com.ghanaradios.kduo.helpers.LogHelper;
import com.ghanaradios.kduo.helpers.PermissionHelper;
import com.ghanaradios.kduo.helpers.SleepTimerService;
import com.ghanaradios.kduo.helpers.TransistorKeys;
import com.google.android.gms.ads.AdListener;
import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.AdSize;
import com.google.android.gms.ads.AdView;
import com.google.android.gms.ads.NativeExpressAdView;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.firebase.analytics.FirebaseAnalytics;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.google.firebase.remoteconfig.FirebaseRemoteConfig;
import com.google.firebase.remoteconfig.FirebaseRemoteConfigSettings;
import com.squareup.picasso.Picasso;

import java.io.File;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.HashMap;
import java.util.Locale;
import java.util.Map;
import java.util.concurrent.TimeUnit;

public final class MainActivityFragment extends Fragment implements SearchView.OnQueryTextListener {


    // A Native Express ad is placed in every nth position in the RecyclerView.
    public static final int ITEMS_PER_AD = 8;
    public static int START_AD_ITEM = 300;

    // The Native Express ad height.
    private static final int NATIVE_EXPRESS_AD_HEIGHT = 85;

    // The Native Express ad unit ID.
//    private static final String AD_UNIT_ID = "ca-app-pub-3231405590589504/9949488007";

    /* Define log tag */
    private static final String LOG_TAG = MainActivityFragment.class.getSimpleName();
    public static String CURRENT_RADIO = "current_radio";
    public AdView adView;
    private ArrayList<Object> customStations;
    /* Main class variables */
    private Application mApplication;
    private Activity mActivity;
    public static CollectionAdapter mCollectionAdapter = null;
    private File mFolder;
    private int mFolderSize;
    private View mRootView;
    private View mActionCallView;
    private RecyclerView mRecyclerView;
    private RecyclerView.LayoutManager mLayoutManager;
    private Parcelable mListState;
    private BroadcastReceiver mCollectionChangedReceiver;
    private BroadcastReceiver mImageChangeRequestReceiver;
    private BroadcastReceiver mPlaybackStateChangedReceiver;
    private int mStationIDSelected;
    private int mStationIDCurrent;
    private int mStationIDLast;
    private String currentPlayingStation;
    private int mTempStationID;
    private Station mTempStation;
    private Uri mNewStationUri;
    private boolean mTwoPane;
    private boolean mPlayback;
    private TextView stationStatus;
    private ImageView stationStatusImage;
    private ActionBar mActionBar;
    private SearchView mSearchView = null;
    private boolean reversed = false;
    private boolean shoFavourite = false;
    private SharedPreferences prefs;
    private MyRadio myRadio;
    private Menu menu;
    FloatingActionButton fab;
    public static ArrayList<Station> shortingStation;
    private FirebaseAnalytics mFirebaseAnalytics;
    private FirebaseRemoteConfig mFirebaseRemoteConfig;
    private String experiment1_variant;


    boolean sortingFlag = true;
    private boolean mSleepTimerRunning;
    private SleepTimerService mSleepTimerService;
    private String mSleepTimerNotificationMessage;
    private Snackbar mSleepTimerNotification;



    /* Constructor (default) */
    public MainActivityFragment() {
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

//        MobileAds.initialize(getActivity(), "ca-app-pub-3231405590589504/1687854601");

        mActionBar = ((MainActivity) getActivity()).actionBar;

        myRadio = MyRadio.getInstance();
        // fragment has options menu_logout
        setHasOptionsMenu(true);

        // get activity and application contexts
        mActivity = getActivity();
        mApplication = mActivity.getApplication();

        prefs = mActivity.getSharedPreferences("com.suman.radio.app.tms", Context.MODE_PRIVATE);

        // set list state null
        mListState = null;

        // initialize id of currently selected station
        mStationIDSelected = 0;

        // initialize temporary station image id
        mTempStationID = -1;

        // initialize two pane
        mTwoPane = false;

        // load playback state
        loadAppState(mActivity);

        // create collection adapter
        if (mCollectionAdapter == null) {


            FirebaseDatabase database = FirebaseDatabase.getInstance();
            DatabaseReference myRef = database.getReference("stations/ghana_radio");

            // Read from the database
            myRef.addValueEventListener(new ValueEventListener() {
                @Override
                public void onDataChange(DataSnapshot dataSnapshot) {


                    customStations = new ArrayList<Object>();

                    String[] favourites = getFravouteStation();

                    for (DataSnapshot station : dataSnapshot.getChildren()) {

                        AppStation s = station.getValue(AppStation.class);

                        if (s != null) {


                            if (Arrays.asList(favourites).contains(s.getRadio_title())) {
                                customStations.add(new Station(new CustomStation(s.getRadio_title(), s.getStream_url(), s.getImage_url(), true)));
                            } else {
                                customStations.add(new Station(new CustomStation(s.getRadio_title(), s.getStream_url(), s.getImage_url())));
                            }

                            addNativeExpressAds();
                            setUpAndLoadNativeExpressAds();

                        }
                    }


                    shortingStation = new ArrayList<Station>();
                    for (Object stn : customStations) {

                        Station thisStation = (Station) stn;
                        shortingStation.add(thisStation);
                    }

                    Collections.sort(shortingStation, new Comparator<Station>() {
                        @Override
                        public int compare(Station station, Station t1) {
                            return station.getStationName().compareToIgnoreCase(t1.getStationName());
                        }
                    });

                    customStations = new ArrayList<Object>();
                    for (Station stn : shortingStation) {
                        Object nweObj = (Object) stn;
                        customStations.add(nweObj);
                    }


                    mCollectionAdapter = new CollectionAdapter(mActivity, customStations, shortingStation);

                    addNativeExpressAds();
                    setUpAndLoadNativeExpressAds();
                    mLayoutManager = new GridLayoutManager(getActivity(), 2);
                    mRecyclerView.setLayoutManager(mLayoutManager);
//                    ItemOffsetDecoration itemDecoration = new ItemOffsetDecoration(getActivity(), R.dimen.item_offset);
//                    mRecyclerView.addItemDecoration(itemDecoration);

//
                    mRecyclerView.setAdapter(mCollectionAdapter);

//                    Collections.reverse(customStations);
                    mCollectionAdapter.notifyDataSetChanged();
                    toggleActionCall();
                    setLoadingView();

                }

                @Override
                public void onCancelled(DatabaseError error) {
                    // Failed to read value
                    Log.w("TAG", "Failed to read value.", error.toException());
                }
            });


        }


        // initialize broadcast receivers
        initializeBroadcastReceivers();

    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        // get list state from saved instance
        if (savedInstanceState != null) {
            mListState = savedInstanceState.getParcelable(TransistorKeys.INSTANCE_LIST_STATE);
        }

        // inflate root view from xml
        mRootView = inflater.inflate(R.layout.fragment_main, container, false);

        mSleepTimerNotificationMessage = getResources().getString(R.string.snackbar_message_timer_set) + " ";
        // initiate sleep timer service
        mSleepTimerService = new SleepTimerService();

        adView = (AdView) mRootView.findViewById(R.id.myAds);

        AdRequest mAdRequest = new AdRequest.Builder()
                .addTestDevice("07CDF722D23887F7936A5DD19CF714D0")
                .build();

        adView.loadAd(mAdRequest);


        // get reference to action call view from inflated root view
        mActionCallView = mRootView.findViewById(R.id.main_actioncall_layout);

        fab= (FloatingActionButton) mRootView.findViewById(R.id.fab);

        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (reversed) {
                    reversed = false;
                } else {
                    reversed = true;
                }
                if (mCollectionAdapter != null)
                    mCollectionAdapter.filterFavourites(reversed);
//                Intent chatIntent = new Intent(getActivity(), LoginActivity.class);
//                startActivity(chatIntent);

            }
        });
        stationStatus = (TextView) mRootView.findViewById(R.id.TV_chooseStation);
        stationStatusImage = (ImageView) mRootView.findViewById(R.id.IV_musicBarIcon);


        // get reference to recycler list view from inflated root view
        mRecyclerView = (RecyclerView) mRootView.findViewById(R.id.main_recyclerview_collection);

        // use this setting to improve performance if you know that changes
        // in content do not change the layout size of the RecyclerView
        // TODO check if necessary here
        mRecyclerView.setHasFixedSize(true);

        // set animator
        mRecyclerView.setItemAnimator(new DefaultItemAnimator());

        // use a linear layout manager
        mLayoutManager = new GridLayoutManager(mActivity, 2);
        mRecyclerView.setLayoutManager(mLayoutManager);
//        ItemOffsetDecoration itemDecoration = new ItemOffsetDecoration(getActivity(), R.dimen.item_offset);
//        mRecyclerView.addItemDecoration(itemDecoration);

        // attach adapter to list view
        mRecyclerView.setAdapter(mCollectionAdapter);


//        toggleActionCall();
        return mRootView;
    }

    private void setLoadingView() {


        if (customStations != null && mPlayback) {

            Station nStation = null;
            int currentStation = 0;
//            String currentRadio = prefs.getString(CURRENT_RADIO, "false--");

            String currentRadio = currentPlayingStation;

            for (int i = 0; i < customStations.size(); i++) {

                if (customStations.get(i) instanceof Station) {
                    Station stnn = (Station) customStations.get(i);

                    if (stnn.getPlaybackState()) {
                        nStation = stnn;
                        currentStation = i;
                    }
                    if (currentRadio.equals(stnn.getStationName())) {
                        nStation = stnn;
                        currentStation = i;
                    }

                }

            }


            if (nStation != null) {

                prefs.edit().putString(CURRENT_RADIO, nStation.getStationName()).apply();
                stationStatus.setText("Now playing: " + nStation.getStationName());

                stationStatusImage.setBackgroundResource(R.drawable.music_bar);

                Picasso.with(mActivity).load(R.drawable.music_bar).fit().placeholder(R.drawable.music_bar).into(stationStatusImage);

                final Station finalNStation = nStation;
                final int finalCurrentStation = currentStation;
                stationStatus.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {

//                        setupRemoteConfig();
                        showAds();
                        Intent intent = new Intent(mActivity, PlayerActivity.class);
                        intent.setAction(TransistorKeys.ACTION_SHOW_PLAYER);
                        intent.putExtra(TransistorKeys.EXTRA_STATION, finalNStation);
                        intent.putExtra(TransistorKeys.EXTRA_STATION_ID, finalCurrentStation);
                        mActivity.startActivity(intent);
//                        }
                    }
                });


            }
        }
    }

    private void showAds() {
        if (myRadio.mInterstitialAd.isLoaded()) {
            myRadio.mInterstitialAd.show();
        }
    }

    @Override
    public void onResume() {
        super.onResume();

        if (customStations != null) {
            loadAppState(mActivity);
            setLoadingView();
        }


        // handles the activity's intent
        Intent intent = mActivity.getIntent();
        if (Intent.ACTION_VIEW.equals(intent.getAction())) {
            handleStreamingLink(intent);
        } else if (TransistorKeys.ACTION_SHOW_PLAYER.equals(intent.getAction())) {
            handleShowPlayer(intent);
        }


        if (mCollectionAdapter != null) {
            // refresh app state
            loadAppState(mActivity);

            // update collection adapter
            mCollectionAdapter.setTwoPane(mTwoPane);
            mCollectionAdapter.refresh();
            if (mCollectionAdapter.getItemCount() > 0) {

                try {
                    mCollectionAdapter.setStationIDSelected(mStationIDSelected, mPlayback, false);


                } catch (Exception e) {
                    e.printStackTrace();
                }
            }


            // show call to action, if necessary
            toggleActionCall();


        }
    }

    @Override
    public void onDestroy() {
//        if (adView != null) {
//            adView.destroy();
//        }
        super.onDestroy();
        unregisterBroadcastReceivers();
        prefs.edit().putString(CURRENT_RADIO, "false--").apply();
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        switch (item.getItemId()) {

//         Search
            case R.id.action_search:
                MenuItem searchItem = menu.findItem(R.id.action_search);
                SearchManager searchManager = (SearchManager) getActivity().getSystemService(Context.SEARCH_SERVICE);
                mSearchView = (SearchView) searchItem.getActionView();
                mSearchView.setSearchableInfo(searchManager.getSearchableInfo(getActivity().getComponentName()));
                mSearchView.setIconifiedByDefault(true);
                mSearchView.setOnQueryTextListener(this);
                return true;

            case R.id.action_a_z:
                if (!sortingFlag) {
                    mCollectionAdapter.sortStationsReverse();
                    sortingFlag = true;
                }

//                ArrayList<Station> stationss = (ArrayList<Station>) (ArrayList<?>) customStations;
//
//                Collections.sort(stationss, new Comparator<Station>() {
//                    @Override
//                    public int compare(Station station, Station t1) {
//                        return station.getStationName().compareToIgnoreCase(t1.getStationName());
//                    }
//                });
//                customStations = (ArrayList<Object>) (ArrayList<?>) stationss;
//
//                mCollectionAdapter.sortStations(customStations);
                return true;
            case R.id.action_z_a:

                if (sortingFlag) {
                    mCollectionAdapter.sortStationsReverse();
                    sortingFlag = false;
                }
                return true;

            case R.id.navItemSleepTimer:

                final Calendar now = Calendar.getInstance();
                final TimePickerDialog timePickerDialog = new TimePickerDialog(getActivity(), android.R.style.Theme_Holo_Light_Dialog, new TimePickerDialog.OnTimeSetListener() {
                    @Override
                    public void onTimeSet(TimePicker view, int hourOfDay, int minute) {

                        String chosenTime = convertTo12Hour(hourOfDay + "", minute + "");
                        String currentTime = convertTo12Hour(now.get(Calendar.HOUR_OF_DAY) + "", now.get(Calendar.MINUTE) + "");

                        if (checkTimeDifference(chosenTime, currentTime)) {
                            Toast.makeText(getActivity().getApplicationContext(), "Selected time must be greater current time! ", Toast.LENGTH_SHORT).show();

                        } else {

                            Calendar thatTime = Calendar.getInstance();
                            thatTime.set(Calendar.DAY_OF_MONTH, now.get(Calendar.DAY_OF_MONTH));
                            thatTime.set(Calendar.MONTH, now.get(Calendar.MONTH)); // 0-11 so 1 less
                            thatTime.set(Calendar.YEAR, now.get(Calendar.YEAR));
                            thatTime.set(Calendar.HOUR_OF_DAY, hourOfDay);
                            thatTime.set(Calendar.MINUTE, minute);

                            Calendar today = Calendar.getInstance();

                            long timeDifference = thatTime.getTimeInMillis() - today.getTimeInMillis();
                            handleMenuSleepTimerClick(timeDifference);
                        }


                    }
                }, now.get(Calendar.HOUR_OF_DAY), now.get(Calendar.MINUTE), false);
                timePickerDialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
                timePickerDialog.getWindow().setType(WindowManager.LayoutParams.TYPE_TOAST);
                timePickerDialog.show();

                return true;

            default:
                return super.onOptionsItemSelected(item);
        }
    }

    private void handleMenuSleepTimerClick(long duration) {
        // load app state
        loadAppState(getActivity().getApplicationContext());

        // CASE: No station is playing, no timer is running
        if (!mPlayback && !mSleepTimerRunning) {
            // unable to start timer
            Toast.makeText(getActivity().getApplicationContext(), getResources().getString(R.string.toastmessage_timer_start_unable), Toast.LENGTH_SHORT).show();
        }
        // CASE: A station is playing, no sleep timer is running
        else if (mPlayback && !mSleepTimerRunning) {
            startSleepTimer(duration);
            Toast.makeText(getActivity().getApplicationContext(), getResources().getString(R.string.toastmessage_timer_activated), Toast.LENGTH_SHORT).show();
        }
        // CASE: A station is playing, Sleep timer is running
        else if (mPlayback) {
            startSleepTimer(duration);
            Toast.makeText(getActivity().getApplicationContext(), getResources().getString(R.string.toastmessage_timer_duration_increased) + " [+" + getReadableTime(duration) + "]", Toast.LENGTH_SHORT).show();
        }

    }

    private void startSleepTimer(long duration) {
        System.out.println(mSleepTimerService + "value");
        // start timer service
        if (mSleepTimerService == null) {
            mSleepTimerService = new SleepTimerService();
        }
        mSleepTimerService.startActionStart(getActivity().getApplicationContext(), duration);

        // show timer notification
        showSleepTimerNotification(duration);
        mSleepTimerRunning = true;
        LogHelper.v(LOG_TAG, "Starting timer service and notification.");
    }
    private void showSleepTimerNotification(long remainingTime) {

        // set snackbar message
        String message;
        if (remainingTime > 0) {
            message = mSleepTimerNotificationMessage + getReadableTime(remainingTime);
        } else {
            message = mSleepTimerNotificationMessage;
        }

        // show snackbar
        mSleepTimerNotification = Snackbar.make(getView(), message, Snackbar.LENGTH_INDEFINITE);
        mSleepTimerNotification.setAction(R.string.dialog_generic_button_cancel, new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                // stop sleep timer service
                mSleepTimerService.startActionStop(getActivity().getApplicationContext());
                mSleepTimerRunning = false;
                saveAppState(getActivity().getApplicationContext());
                // notify user
                Toast.makeText(getActivity().getApplicationContext(), getResources().getString(R.string.toastmessage_timer_cancelled), Toast.LENGTH_SHORT).show();
                LogHelper.v(LOG_TAG, "Sleep timer cancelled.");
            }
        });
        mSleepTimerNotification.show();

    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        // save list view position
        mListState = mLayoutManager.onSaveInstanceState();
        outState.putParcelable(TransistorKeys.INSTANCE_LIST_STATE, mListState);
        super.onSaveInstanceState(outState);
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);

        switch (requestCode) {
            case TransistorKeys.PERMISSION_REQUEST_IMAGE_PICKER_READ_EXTERNAL_STORAGE: {
                if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    // permission granted - get system picker for images
                    Intent pickImageIntent = new Intent(Intent.ACTION_PICK, android.provider.MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
                    mActivity.startActivityForResult(pickImageIntent, TransistorKeys.REQUEST_LOAD_IMAGE);
                } else {
                    // permission denied
                    Toast.makeText(mActivity, mActivity.getString(R.string.toastalert_permission_denied) + " READ_EXTERNAL_STORAGE", Toast.LENGTH_LONG).show();
                }
                break;
            }

            case TransistorKeys.PERMISSION_REQUEST_STATION_FETCHER_READ_EXTERNAL_STORAGE: {
                if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    // permission granted - fetch station from given Uri
//                    fetchNewStation(mNewStationUri);
                } else {
                    // permission denied
                    Toast.makeText(mActivity, mActivity.getString(R.string.toastalert_permission_denied) + " READ_EXTERNAL_STORAGE", Toast.LENGTH_LONG).show();
                }
                break;
            }
        }
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        // retrieve selected image Uri from image picker
        Uri newImageUri = null;
        if (null != data) {
            newImageUri = data.getData();
        }

        if (requestCode == TransistorKeys.REQUEST_LOAD_IMAGE && resultCode == Activity.RESULT_OK && newImageUri != null) {


        } else {
            LogHelper.e(LOG_TAG, "Unable to get image from media picker. Did not receive an Uri");
        }
    }

    /* Show or hide call to action view if necessary */
    private void toggleActionCall() {
        // show call to action, if necessary
        if (customStations != null && customStations.size() == 0) {
            mActionCallView.setVisibility(View.VISIBLE);
            mRecyclerView.setVisibility(View.GONE);
        } else {
            mActionCallView.setVisibility(View.GONE);
            mRecyclerView.setVisibility(View.VISIBLE);
        }
    }

    /* Check permissions and start image picker */
    private void selectFromImagePicker() {
        // request read permissions
        PermissionHelper permissionHelper = new PermissionHelper(mActivity, mRootView);
        if (permissionHelper.requestReadExternalStorage(TransistorKeys.PERMISSION_REQUEST_IMAGE_PICKER_READ_EXTERNAL_STORAGE)) {
            // get system picker for images
            Intent pickImageIntent = new Intent(Intent.ACTION_PICK, android.provider.MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
            startActivityForResult(pickImageIntent, TransistorKeys.REQUEST_LOAD_IMAGE);
        }
    }

    /* Handles tap on streaming link */
    private void handleStreamingLink(Intent intent) {
        mNewStationUri = intent.getData();

        // clear the intent
        intent.setAction("");

        // check for null and type "http"
        if (mNewStationUri != null && mNewStationUri.getScheme().startsWith("http")) {
            // download and add new station
//            fetchNewStation(mNewStationUri);
        } else if (mNewStationUri != null && mNewStationUri.getScheme().startsWith("file")) {
            // check for read permission
            PermissionHelper permissionHelper = new PermissionHelper(mActivity, mRootView);
            if (permissionHelper.requestReadExternalStorage(TransistorKeys.PERMISSION_REQUEST_STATION_FETCHER_READ_EXTERNAL_STORAGE)) {
                // read and add new station
//                fetchNewStation(mNewStationUri);
            }
        }
        // unsuccessful - log failure
        else {
            LogHelper.v(LOG_TAG, "Received an empty intent");
        }
    }


    /* Handles intent to show player from notification or from shortcut */
    private void handleShowPlayer(Intent intent) {
        // get station from intent
        Station station = null;
        if (intent.hasExtra(TransistorKeys.EXTRA_STATION)) {
            // get station from notification
            station = intent.getParcelableExtra(TransistorKeys.EXTRA_STATION);
            prefs.edit().putString(CURRENT_RADIO, station.getStationName()).apply();

        } else if (intent.hasExtra(TransistorKeys.EXTRA_STREAM_URI)) {
            // get Uri of station from home screen shortcut
            station = mCollectionAdapter.findStation(Uri.parse(intent.getStringExtra(TransistorKeys.EXTRA_STREAM_URI)));
        } else if (intent.hasExtra(TransistorKeys.EXTRA_LAST_STATION) && intent.getBooleanExtra(TransistorKeys.EXTRA_LAST_STATION, false)) {
            // try to get last station
            loadAppState(mActivity);
            if (mStationIDLast > -1 && mStationIDLast < mCollectionAdapter.getItemCount()) {
                station = (Station) mCollectionAdapter.getStation(mStationIDLast);
            }
        }

        if (station == null) {
            Toast.makeText(mActivity, getString(R.string.toastalert_station_not_found), Toast.LENGTH_LONG).show();
        }

        // get playback action from intent
        boolean startPlayback;
        if (intent.hasExtra(TransistorKeys.EXTRA_PLAYBACK_STATE)) {
            startPlayback = intent.getBooleanExtra(TransistorKeys.EXTRA_PLAYBACK_STATE, false);
        } else {
            startPlayback = false;
        }

//
//        setupRemoteConfig();
        showAds();
        Intent playerIntent = new Intent(mActivity, PlayerActivity.class);
        playerIntent.setAction(TransistorKeys.ACTION_SHOW_PLAYER);
        playerIntent.putExtra(TransistorKeys.EXTRA_STATION, station);
        playerIntent.putExtra(TransistorKeys.EXTRA_PLAYBACK_STATE, startPlayback);
        startActivity(playerIntent);
//        }
    }


    /* Translates milliseconds into minutes and seconds */
    private String getReadableTime(long remainingTime) {
        return String.format(Locale.getDefault(), "%02d:%02d",
                TimeUnit.MILLISECONDS.toMinutes(remainingTime),
                TimeUnit.MILLISECONDS.toSeconds(remainingTime) -
                        TimeUnit.MINUTES.toSeconds(TimeUnit.MILLISECONDS.toMinutes(remainingTime)));
    }

    /* Loads app state from preferences */
    private void loadAppState(Context context) {
        SharedPreferences settings = PreferenceManager.getDefaultSharedPreferences(context);
        mStationIDSelected = settings.getInt(TransistorKeys.PREF_STATION_ID_SELECTED, 0);
        mStationIDCurrent = settings.getInt(TransistorKeys.PREF_STATION_ID_CURRENTLY_PLAYING, -1);
        mStationIDLast = settings.getInt(TransistorKeys.PREF_STATION_ID_LAST, -1);
        currentPlayingStation = settings.getString(TransistorKeys.PREF_STATION_NAME_CURRENTLY_PLAYING, "Choose a station below to begin..");
        mPlayback = settings.getBoolean(TransistorKeys.PREF_PLAYBACK, false);
        mTwoPane = settings.getBoolean(TransistorKeys.PREF_TWO_PANE, false);
        LogHelper.v(LOG_TAG, "Loading state (" + mStationIDCurrent + " / " + mStationIDLast + " / " + mPlayback + ")");
    }

    /* Saves app state to SharedPreferences */
    private void saveAppState(Context context) {
        SharedPreferences settings = PreferenceManager.getDefaultSharedPreferences(context);
        SharedPreferences.Editor editor = settings.edit();
        editor.putInt(TransistorKeys.PREF_STATION_ID_CURRENTLY_PLAYING, mStationIDCurrent);
        editor.putInt(TransistorKeys.PREF_STATION_ID_LAST, mStationIDLast);
        editor.putString(TransistorKeys.PREF_STATION_NAME_CURRENTLY_PLAYING, currentPlayingStation);
        editor.putBoolean(TransistorKeys.PREF_PLAYBACK, mPlayback);
        editor.apply();
        LogHelper.v(LOG_TAG, "Saving state (" + mStationIDCurrent + " / " + mStationIDLast + " / " + mPlayback + ")");
    }

    private String[] getFravouteStation() {
        SharedPreferences settings = PreferenceManager.getDefaultSharedPreferences(mActivity);
        String[] tempPlayList = settings.getString(TransistorKeys.PREF_FAVORITE_STATION, "").split(",");
        return tempPlayList;

    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        super.onCreateOptionsMenu(menu, inflater);
        this.menu = menu;
        menu.clear();
        inflater.inflate(R.menu.menu_main_actionbar, menu);
        MenuItem searchItem = menu.findItem(R.id.action_search);
        SearchManager searchManager = (SearchManager) getActivity().getSystemService(Context.SEARCH_SERVICE);
        mSearchView = (SearchView) searchItem.getActionView();
        mSearchView.setSearchableInfo(searchManager.getSearchableInfo(getActivity().getComponentName()));
        mSearchView.setIconifiedByDefault(true);
        mSearchView.setOnQueryTextListener(this);

    }

    /* Initializes broadcast receivers for onCreate */
    private void initializeBroadcastReceivers() {

        // RECEIVER: state of playback has changed
        mPlaybackStateChangedReceiver = new BroadcastReceiver() {
            @Override
            public void onReceive(Context context, Intent intent) {
                if (intent.hasExtra(TransistorKeys.EXTRA_PLAYBACK_STATE_CHANGE)) {
                    handlePlaybackStateChanges(intent);
                }
            }
        };
        IntentFilter playbackStateChangedIntentFilter = new IntentFilter(TransistorKeys.ACTION_PLAYBACK_STATE_CHANGED);
        LocalBroadcastManager.getInstance(mActivity).registerReceiver(mPlaybackStateChangedReceiver, playbackStateChangedIntentFilter);

        // RECEIVER: station added, deleted, or changed
        mCollectionChangedReceiver = new BroadcastReceiver() {
            @Override
            public void onReceive(Context context, Intent intent) {
                if (intent != null && intent.hasExtra(TransistorKeys.EXTRA_COLLECTION_CHANGE)) {
//                    handleCollectionChanges(intent);
                }
            }
        };
        IntentFilter collectionChangedIntentFilter = new IntentFilter(TransistorKeys.ACTION_COLLECTION_CHANGED);
        LocalBroadcastManager.getInstance(mApplication).registerReceiver(mCollectionChangedReceiver, collectionChangedIntentFilter);

        // RECEIVER: listen for request to change station image
        mImageChangeRequestReceiver = new BroadcastReceiver() {
            @Override
            public void onReceive(Context context, Intent intent) {
                if (intent.hasExtra(TransistorKeys.EXTRA_STATION) && intent.hasExtra(TransistorKeys.EXTRA_STATION_ID)) {
                    // get station and id from intent
                    mTempStation = intent.getParcelableExtra(TransistorKeys.EXTRA_STATION);
                    mTempStationID = intent.getIntExtra(TransistorKeys.EXTRA_STATION_ID, -1);
                    // start image picker
                    selectFromImagePicker();
                }
            }
        };
        IntentFilter imageChangeRequestIntentFilter = new IntentFilter(TransistorKeys.ACTION_IMAGE_CHANGE_REQUESTED);
        LocalBroadcastManager.getInstance(mApplication).registerReceiver(mImageChangeRequestReceiver, imageChangeRequestIntentFilter);


    }


    /* Unregisters broadcast receivers */
    private void unregisterBroadcastReceivers() {
        LocalBroadcastManager.getInstance(mActivity).unregisterReceiver(mPlaybackStateChangedReceiver);
        LocalBroadcastManager.getInstance(mActivity).unregisterReceiver(mCollectionChangedReceiver);
        LocalBroadcastManager.getInstance(mActivity).unregisterReceiver(mImageChangeRequestReceiver);
    }

    /* Handles changes in state of playback, eg. start, stop, loading stream */
    private void handlePlaybackStateChanges(Intent intent) {
        switch (intent.getIntExtra(TransistorKeys.EXTRA_PLAYBACK_STATE_CHANGE, 1)) {
            // CASE: playback was stopped
            case TransistorKeys.PLAYBACK_STOPPED:
                // load app state
                loadAppState(mActivity);
                // stop sleep timer

                break;
        }
    }


    @Override
    public boolean onQueryTextSubmit(String query) {
        return false;
    }

    @Override
    public boolean onQueryTextChange(String newText) {
        if (TextUtils.isEmpty(newText)) {
            if (mCollectionAdapter != null)
                mCollectionAdapter.getFilter().filter("");
        } else {
            if (mCollectionAdapter != null)
                mCollectionAdapter.getFilter().filter(newText.toString());
        }
        return true;
    }


    private boolean checkTimeDifference(String chosenTime, String currentTime) {


        DateFormat f1 = new SimpleDateFormat("h:mm a");


        Date __startTime = null;
        Date __endTime = null;
        try {
            __startTime = f1.parse(currentTime);
            __endTime = f1.parse(chosenTime);

        } catch (ParseException e) {
            e.printStackTrace();
        }

        if (__startTime != null && __endTime != null) {
            if (__endTime.before(__startTime) || __startTime.equals(__endTime)) {
                return true;
            }
        } else {
            return true;
        }
        return false;

    }


    private String convertTo12Hour(String hourOfDay, String minueOfDay) {


        String selectedDate = "11:00 AM";
        try {
            String s = hourOfDay + ":" + minueOfDay + ":00";
            DateFormat f1 = new SimpleDateFormat("HH:mm:ss"); //HH for hour of the day (0 - 23)
            Date d = f1.parse(s);
            DateFormat f2 = new SimpleDateFormat("h:mm a");
            selectedDate = f2.format(d).toUpperCase();
        } catch (Exception ex) {
            ex.printStackTrace();
        }

        return selectedDate;

    }


    private void addNativeExpressAds() {

        // Loop through the items array and place a new Native Express ad in every ith position in
        // the items List.
        for (int i = START_AD_ITEM; i <= customStations.size(); i += ITEMS_PER_AD) {
            Activity thisActivity = getActivity();
            if (thisActivity != null) {
                final NativeExpressAdView adView = new NativeExpressAdView(getActivity());
                customStations.add(i, adView);
            }

        }
    }


    /**
     * Sets up and loads the Native Express ads.
     */
    private void setUpAndLoadNativeExpressAds() {
        // Use a Runnable to ensure that the RecyclerView has been laid out before setting the
        // ad size for the Native Express ad. This allows us to set the Native Express ad's
        // width to match the full width of the RecyclerView.
        mRecyclerView.post(new Runnable() {
            @Override
            public void run() {
                final float scale = myRadio.getResources().getDisplayMetrics().density;
                // Set the ad size and ad unit ID for each Native Express ad in the items list.

                for (int i = 0; i < customStations.size(); i++) {
                    if (customStations.get(i) instanceof NativeExpressAdView) {
                        final NativeExpressAdView adView =
                                (NativeExpressAdView) customStations.get(i);
//                    final CardView cardView = (CardView) findViewById(R.id.ad_card_view);
                        CardView cardView = (CardView) getActivity().findViewById(R.id.ad_card_view);
                        if (cardView != null) {
                            final int adWidth = cardView.getWidth() - cardView.getPaddingLeft()
                                    - cardView.getPaddingRight();
                            AdSize adSize = new AdSize((int) (adWidth / scale), NATIVE_EXPRESS_AD_HEIGHT);
                            adView.setAdSize(adSize);
                        } else {

                            DisplayMetrics displaymetrics = new DisplayMetrics();
                            getActivity().getWindowManager().getDefaultDisplay().getMetrics(displaymetrics);
                            int height = displaymetrics.heightPixels;
                            int width = displaymetrics.widthPixels;

                            final int adWidth = width - 20;
                            AdSize adSize = new AdSize((int) (adWidth / scale), NATIVE_EXPRESS_AD_HEIGHT);
                            adView.setAdSize(adSize);

                        }
                        adView.setAdUnitId(getResources().getString(R.string.admobAppID));
                    }
                }

                for (int i = 0; i < customStations.size(); i++) {
                    if (customStations.get(i) instanceof NativeExpressAdView) {
                        START_AD_ITEM = i;
                        // Load the first Native Express ad in the items list.
                        loadNativeExpressAd(START_AD_ITEM);
                        return;
                    }
                }

            }
        });
    }


    /**
     * Loads the Native Express ads in the items list.
     */
    private void loadNativeExpressAd(final int index) {

        if (index >= customStations.size()) {
            return;
        }

        Object item = customStations.get(index);
        if (item instanceof NativeExpressAdView) {
            if (!(item instanceof NativeExpressAdView)) {
                throw new ClassCastException("Expected item at index " + index + " to be a Native"
                        + " Express ad.");
            }

            final NativeExpressAdView adView = (NativeExpressAdView) item;

            // Set an AdListener on the NativeExpressAdView to wait for the previous Native Express ad
            // to finish loading before loading the next ad in the items list.
            adView.setAdListener(new AdListener() {
                @Override
                public void onAdLoaded() {
                    super.onAdLoaded();
                    // The previous Native Express ad loaded successfully, call this method again to
                    // load the next ad in the items list.
                    for (int i = index; i < customStations.size(); ++i) {
                        if (customStations.get(i) instanceof NativeExpressAdView) {
                            loadNativeExpressAd(++i);
                            return;

                        }
                    }
                }

                @Override
                public void onAdFailedToLoad(int errorCode) {
                    // The previous Native Express ad failed to load. Call this method again to load
                    // the next ad in the items list.
                    Log.e("MainActivity", "The previous Native Express ad failed to load. Attempting to"
                            + " load the next Native Express ad in the items list.");
//                    loadNativeExpressAd(index + ITEMS_PER_AD);

                    for (int i = index; i < customStations.size(); ++i) {
                        if (customStations.get(i) instanceof NativeExpressAdView) {
                            loadNativeExpressAd(++i);
                            return;
                        }
                    }
                }
            });

            // Load the Native Express ad.
            adView.loadAd(new AdRequest.Builder().build());
        } else {
            int i = index;
            if (i < customStations.size())
                loadNativeExpressAd(++i);
        }
    }


    private void setupRemoteConfig() {
        mFirebaseAnalytics = FirebaseAnalytics.getInstance(getActivity());
        mFirebaseRemoteConfig = FirebaseRemoteConfig.getInstance();

        FirebaseRemoteConfigSettings configSettings = new FirebaseRemoteConfigSettings
                .Builder()
                .setDeveloperModeEnabled(BuildConfig.DEBUG)
                .build();
        mFirebaseRemoteConfig.setConfigSettings(configSettings);

        Map remoteConfigDefaults = new HashMap<String, Object>();
        remoteConfigDefaults.put("promoted_bundle", "basic");
        mFirebaseRemoteConfig.setDefaults(remoteConfigDefaults);

        fetchData();

    }

    private void fetchData() {
        long cacheExpiration = 3600; // 1 hour in seconds.
        if (mFirebaseRemoteConfig.getInfo().getConfigSettings().isDeveloperModeEnabled()) {
            cacheExpiration = 0;
        }


        mFirebaseRemoteConfig.fetch(cacheExpiration).addOnCompleteListener(new OnCompleteListener<Void>() {
            @Override
            public void onComplete(@NonNull com.google.android.gms.tasks.Task<Void> task) {
                if (task.isSuccessful()) {
                    mFirebaseRemoteConfig.activateFetched();
                    String promotedBundle = getPromotedBundle();
                    if (promotedBundle == "premium") {
                        showAds();

                    } else {
                        System.out.println("not premium" + promotedBundle);
                    }
                }


            }


        });
    }

    private String getPromotedBundle() {
        FirebaseAnalytics.getInstance(getActivity()).logEvent("promotion_set", new Bundle());

        FirebaseRemoteConfig config = FirebaseRemoteConfig.getInstance();
        String promotedBundle = config.getString("low_risk_user");
        boolean will_spend = config.getBoolean("low_risk_user");
        System.out.println(will_spend);

        if (promotedBundle.equals("predicted") && will_spend) {
            return "premium";
        } else {
            return promotedBundle;
        }
    }

}

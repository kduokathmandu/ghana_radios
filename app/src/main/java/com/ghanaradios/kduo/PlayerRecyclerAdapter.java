package com.ghanaradios.kduo;

import android.app.Activity;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.annotation.NonNull;
import android.support.v4.content.ContextCompat;
import android.support.v4.content.LocalBroadcastManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.ghanaradios.kduo.core.Station;
import com.ghanaradios.kduo.helpers.LogHelper;
import com.ghanaradios.kduo.helpers.TransistorKeys;

import java.util.ArrayList;

public final class PlayerRecyclerAdapter extends RecyclerView.Adapter<CollectionAdapterViewHolder> {

    private static final int MENU_ITEM_VIEW_TYPE = 0;
    private static final int NATIVE_EXPRESS_AD_VIEW_TYPE = 1;

    private final Activity mActivity;
    private ArrayList<Object> stations;
    private ArrayList<Station> mStationList;
    private int mStationIDSelected;
    private SharedPreferences prefs;
    private String currentPlayingStation;
    private boolean mTwoPane;
    private int mStationIDCurrent;
    private int mStationIDLast;
    private boolean mPlayback;
    private boolean mStationLoading;
    private String tempPlayList;
    StringBuilder sb;
    private static final String LOG_TAG = CollectionAdapter.class.getSimpleName();
    private BroadcastReceiver mPlaybackStateChangedReceiver;

    /* Constructor */
    public PlayerRecyclerAdapter(Activity activity, ArrayList<Object> stations, ArrayList<Station> shortingStation) {
        // set main variables
        mActivity = activity;
        this.stations = stations;
        this.mStationList = shortingStation;
        mStationIDSelected = 0;
        prefs = mActivity.getSharedPreferences("com.suman.radio.app.tms", Context.MODE_PRIVATE);

    }


    @NonNull
    @Override
    public CollectionAdapterViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(mActivity).inflate(R.layout.list_item_collection, parent, false);
        return new CollectionAdapterViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull CollectionAdapterViewHolder holder, int position) {

        if (stations.get(position) instanceof Station) {

            final Station station = (Station) stations.get(position);

            Glide.with(holder.itemView.getContext())
                    .load(station.getStationImageURL())
                    .override(130, 100)
                    .placeholder(R.drawable.music_place)
                    .diskCacheStrategy(DiskCacheStrategy.SOURCE)
                    .dontAnimate()
                    .into(holder.getStationImageView());
            holder.getStationNameView().setText(station.getStationName());
            prefs.edit().putString(MainActivityFragment.CURRENT_RADIO, station.getStationName()).apply();

            holder.getStationFavouriteView().setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {


                    if (station.getCustomStation().isFavourite()) {
                        removeFavoriteStation(mActivity, station.getStationName());
                        station.getCustomStation().setFavourite(false);

                    } else {
                        saveFavoriteStation(mActivity, station.getStationName(), station);

                        station.getCustomStation().setFavourite(true);

                    }
                }
            });


            if (station.getCustomStation().isFavourite()) {
                holder.getStationFavouriteView().setImageDrawable(ContextCompat.getDrawable(mActivity, R.drawable.ic_fav_true));
            } else {
                holder.getStationFavouriteView().setImageDrawable(ContextCompat.getDrawable(mActivity, R.drawable.ic_fav_false));
            }


            holder.setClickListener(new CollectionAdapterViewHolder.ClickListener() {
                @Override
                public void onClick(View v, int pos, boolean isLongClick) {

                    Station stnnn = null;
                    try {
                        mStationIDSelected = pos;
                        stnnn = (Station) stations.get(pos);

                    currentPlayingStation = stnnn.getStationName();
                    saveAppState(mActivity);
                    handleSingleClick(pos, (Station) stations.get(pos));
                    } catch (Exception e) {
                        e.printStackTrace();
                    }

                }
            });
        }

    }

    private void handleSingleClick(int position, Station station) {

        Bundle args = new Bundle();
        args.putParcelable(TransistorKeys.ARG_STATION, station);

        args.putInt(TransistorKeys.ARG_STATION_ID, position);
//        args.putBoolean(TransistorKeys.ARG_TWO_PANE, mTwoPane);
        args.putParcelableArrayList("mStationList", mStationList);

        PlayerActivityFragment playerActivityFragment = new PlayerActivityFragment();
        playerActivityFragment.setArguments(args);
        mActivity.getFragmentManager().beginTransaction()
                .replace(R.id.player_container, playerActivityFragment, TransistorKeys.PLAYER_FRAGMENT_TAG)
                .commitAllowingStateLoss();

        notifyDataSetChanged();
    }

    @Override
    public int getItemCount() {
        return stations.size();
    }

    @Override
    public void onAttachedToRecyclerView(RecyclerView recyclerView) {
        super.onAttachedToRecyclerView(recyclerView);
        // load state
        loadAppState(mActivity);
        // initialize broadcast receivers
        initializeBroadcastReceivers();
    }

    public void removeFavoriteStation(Context context, String stationName) {
        SharedPreferences settings = PreferenceManager.getDefaultSharedPreferences(context);
        String tempPlayList = settings.getString(TransistorKeys.PREF_FAVORITE_STATION, "");
        stationName = stationName + ",";
        String finalPlaylist = tempPlayList.replaceAll(stationName, "");

        SharedPreferences.Editor editor = settings.edit();
        editor.putString(TransistorKeys.PREF_FAVORITE_STATION, finalPlaylist);
        editor.apply();
        notifyDataSetChanged();


    }

    public void saveFavoriteStation(Context context, String stationName, Station station) {
        SharedPreferences settings = PreferenceManager.getDefaultSharedPreferences(context);
        sb = new StringBuilder();
        tempPlayList = settings.getString(TransistorKeys.PREF_FAVORITE_STATION, "");
        sb.append(tempPlayList);
        if (!sb.toString().toLowerCase().contains(stationName.toLowerCase())) {
            sb.append(stationName).append(","); }
        SharedPreferences.Editor editor = settings.edit();
        editor.putString(TransistorKeys.PREF_FAVORITE_STATION, sb.toString());
        editor.apply();
        editor.commit();
        loadAppState(mActivity);
        notifyDataSetChanged();

    }


    private void loadAppState(Context context) {
        SharedPreferences settings = PreferenceManager.getDefaultSharedPreferences(context);
        mTwoPane = settings.getBoolean(TransistorKeys.PREF_TWO_PANE, false);
        tempPlayList = settings.getString(TransistorKeys.PREF_FAVORITE_STATION, "");
        mStationIDCurrent = settings.getInt(TransistorKeys.PREF_STATION_ID_CURRENTLY_PLAYING, -1);
        mStationIDLast = settings.getInt(TransistorKeys.PREF_STATION_ID_LAST, -1);
        currentPlayingStation = settings.getString(TransistorKeys.PREF_STATION_NAME_CURRENTLY_PLAYING, "Choose a station below to begin..");
        mStationIDSelected = settings.getInt(TransistorKeys.PREF_STATION_ID_SELECTED, 0);
        mPlayback = settings.getBoolean(TransistorKeys.PREF_PLAYBACK, false);
        mStationLoading = settings.getBoolean(TransistorKeys.PREF_STATION_LOADING, false);
        LogHelper.v(LOG_TAG, "Loading state (" + mStationIDCurrent + " / " + mStationIDLast + " / " + mPlayback + " / " + mStationLoading + ")");
    }

    private void saveAppState(Context context) {
        SharedPreferences settings = PreferenceManager.getDefaultSharedPreferences(context);
        SharedPreferences.Editor editor = settings.edit();
        editor.putInt(TransistorKeys.PREF_STATION_ID_SELECTED, mStationIDSelected);
        editor.putString(TransistorKeys.PREF_STATION_NAME_CURRENTLY_PLAYING, currentPlayingStation);
        editor.putBoolean(TransistorKeys.PREF_PLAYBACK, false);

        editor.apply();
        editor.commit();
    }

    private void initializeBroadcastReceivers() {

        // RECEIVER: state of playback has changed
        mPlaybackStateChangedReceiver = new BroadcastReceiver() {
            @Override
            public void onReceive(Context context, Intent intent) {
                if (intent.hasExtra(TransistorKeys.EXTRA_PLAYBACK_STATE_CHANGE)) {
                    handlePlaybackStateChanged(intent);
                }
            }
        };
        IntentFilter playbackStateChangedIntentFilter = new IntentFilter(TransistorKeys.ACTION_PLAYBACK_STATE_CHANGED);
        LocalBroadcastManager.getInstance(mActivity).registerReceiver(mPlaybackStateChangedReceiver, playbackStateChangedIntentFilter);
    }

    private void handlePlaybackStateChanged(Intent intent) {

        // load app state
        loadAppState(mActivity);

        if (intent.hasExtra(TransistorKeys.EXTRA_PLAYBACK_STATE_CHANGE) && intent.hasExtra(TransistorKeys.EXTRA_STATION_ID)) {

            notifyDataSetChanged();

            // get station ID from intent
            int stationID = intent.getIntExtra(TransistorKeys.EXTRA_STATION_ID, 0);
            switch (intent.getIntExtra(TransistorKeys.EXTRA_PLAYBACK_STATE_CHANGE, 1)) {


                // CASE: player is preparing stream
                case TransistorKeys.PLAYBACK_LOADING_STATION:

                    if (mStationIDLast > -1 && mStationIDLast < stations.size()) {
                        if (stations.get(mStationIDLast) instanceof Station) {
                            Station stnn = (Station) stations.get(mStationIDLast);
                            stnn.setPlaybackState(false);
                        }
                    }
                    mStationLoading = true;
                    mPlayback = true;

                    if (stationID > -1 && stationID < stations.size()) {
                        if (stations.get(stationID) instanceof Station) {
                            Station stnn = (Station) stations.get(stationID);
                            stnn.setPlaybackState(true);
                        }
                    }
                    notifyDataSetChanged();

                    break;

                // CASE: playback has started
                case TransistorKeys.PLAYBACK_STARTED:

                    mStationLoading = false;
                    if (stationID > -1 && stationID < stations.size()) {
                        if (stations.get(stationID) instanceof Station) {
                            Station stnn = (Station) stations.get(stationID);
                            stnn.setPlaybackState(true);
                        }
                    }
                    notifyDataSetChanged();
                    break;

                // CASE: playback was stopped
                case TransistorKeys.PLAYBACK_STOPPED:

                    mPlayback = false;
                    if (stationID > -1 && stationID < stations.size()) {
                        if (stations.get(stationID) instanceof Station) {
                            Station stnn = (Station) stations.get(stationID);
                            stnn.setPlaybackState(false);
                        }
                    }
                    notifyDataSetChanged();
                    break;
            }
        }

    }


}

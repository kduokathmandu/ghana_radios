package com.ghanaradios.kduo.chat.chat;

import android.content.Context;
import android.content.SharedPreferences;
import android.graphics.Typeface;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.google.firebase.database.DatabaseReference;
import com.ghanaradios.kduo.R;
import com.ghanaradios.kduo.chat.model.Message;

import java.util.List;

public class MessageAdapter extends RecyclerView.Adapter<MessageAdapter.MyViewHolder> {
    Context context;
    List<Message> messages;
    DatabaseReference messageDb;
    SharedPreferences sharedPreferences;

    public MessageAdapter(Context context, List<Message> messages, DatabaseReference messageDb, SharedPreferences sharedPreferences) {
        this.context = context;
        this.messages = messages;
        this.messageDb = messageDb;
        this.sharedPreferences = sharedPreferences;
    }

//    public MessageAdapter(Context context, List<Message> messages, DatabaseReference messageDb) {
//        this.context = context;
//        this.messages = messages;
//        this.messageDb = messageDb;
//    }

    @NonNull
    @Override
    public MyViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        View view = LayoutInflater.from(context).inflate(R.layout.item_message, viewGroup, false);
        return new MyViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull MyViewHolder holder, int position) {

        Message message = messages.get(position);
        if (message.getUid().equals(sharedPreferences.getString("uid", ""))) {

            holder.tvUsername.setText("You : ");
            holder.tvTitle.setText(message.getMessage());
            holder.tvTitle.setTextColor(context.getResources().getColor(R.color.black));

            holder.tvTitle.setGravity(Gravity.START);
//            holder.l1.setBackgroundColor(context.getResources().getColor(R.color.primary));


        } else {
            holder.tvUsername.setTypeface(null,Typeface.NORMAL);
            holder.tvUsername.setText(message.getName() + " : ");
            holder.tvTitle.setTextColor(context.getResources().getColor(R.color.black));
            holder.tvTitle.setText( message.getMessage());
            holder.ibDelete.setVisibility(View.GONE);
        }

    }

    @Override
    public int getItemCount() {
        return messages.size();
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {
        TextView tvTitle;
        TextView tvUsername;
        ImageButton ibDelete;
        LinearLayout l1;

        public MyViewHolder(@NonNull View itemView) {
            super(itemView);
            tvTitle = (TextView) itemView.findViewById(R.id.tvTitle);
            tvUsername = (TextView) itemView.findViewById(R.id.tvusername);
            ibDelete = (ImageButton) itemView.findViewById(R.id.ibDelete);
            l1 = (LinearLayout) itemView.findViewById(R.id.l1Message);

            ibDelete.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    messageDb.child(messages.get(getAdapterPosition()).getKey()).removeValue();
                }
            });
        }
    }
}

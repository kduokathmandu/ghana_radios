package com.ghanaradios.kduo.activities.RequestRadio;

import android.Manifest;
import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.os.StrictMode;
import android.provider.MediaStore;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AppCompatActivity;
import android.text.Html;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.PopupMenu;
import android.widget.TextView;
import android.widget.Toast;

import com.ghanaradios.kduo.R;
import com.ghanaradios.kduo.helpers.TransistorKeys;


public class RequestRadioActivity extends AppCompatActivity implements View.OnClickListener, PopupMenu.OnMenuItemClickListener {

    TextView stationNameTV;
    TextView streamLinkTV;

    EditText stationNameET;
    EditText streamLinkET;
    EditText topicET;
    EditText genreET;
    EditText websiteET;
    EditText logoET;

    ImageView showGenreImageView;
    ImageView showTopicImageView;
    ImageView addImageImageView;

    Button submitButton;

    boolean imageSelected = false;


    String picturePath;
    String logo;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_request_radio);


        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder().permitAll().build();
        StrictMode.setThreadPolicy(policy);


        bindActivity();
    }

    private void bindActivity() {
        stationNameTV = (TextView) findViewById(R.id.stationNameTextView);
        stationNameTV.setText((Html.fromHtml(TransistorKeys.stationNameText)));

        streamLinkTV = (TextView) findViewById(R.id.streamlinkTextView);
        streamLinkTV.setText(Html.fromHtml(TransistorKeys.streamUrlText));

        showGenreImageView = (ImageView) findViewById(R.id.showGenreImageView);
        showGenreImageView.setOnClickListener(this);

        showTopicImageView = (ImageView) findViewById(R.id.showTopicImageView);
        showTopicImageView.setOnClickListener(this);

        addImageImageView = (ImageView) findViewById(R.id.addImageImageView);
        addImageImageView.setOnClickListener(this);

        stationNameET = (EditText) findViewById(R.id.stationNameET);
        streamLinkET = (EditText) findViewById(R.id.stationUrlET);
        topicET = (EditText) findViewById(R.id.topicET);
        genreET = (EditText) findViewById(R.id.genreET);
        websiteET = (EditText) findViewById(R.id.websiteET);
        logoET = (EditText) findViewById(R.id.logoET);

        submitButton = (Button) findViewById(R.id.submitButton);
        submitButton.setOnClickListener(this);

    }

    @Override
    public void onClick(View v) {

        switch (v.getId()) {
            case R.id.submitButton:


                String stationName = stationNameET.getText().toString();
                String streamUrl = streamLinkET.getText().toString();
                String topic = topicET.getText().toString();
                String genre = genreET.getText().toString();
                String website = websiteET.getText().toString();

                if (stationName.length() <= 0) {
                    stationNameET.setError("This Field is cumpulsory.");
                    break;
                } else if (streamUrl.length() <= 0) {
                    streamLinkET.setError("This Field is cumpulsory.");
                    break;

                } else {

                    logo = logoET.getText().toString();

                    if (logo.length() <= 0) {
                        logo = " ";
                    }


                    try {


                        String body = "Station Name : " + stationName + "\n" +
                                "Station Url : " + streamUrl + "\n" +
                                "Station Topic : " + topic + "\n" +
                                "Station Genre : " + genre + "\n" +
                                "Station Website : " + website + "\n" +
                                "Sent From : " + this.getString(R.string.app_name) + "\n";

                        GMailSender sender = new GMailSender(this, "temp1@kduoapps.com", "#Beetl3bug");
                        sender.sendMail("Radio Station Request",
                                body,
                                "support@kduoapps.com", logo, imageSelected);


                        ViewDialog viewDialog = new ViewDialog();
                        viewDialog.showDialog(RequestRadioActivity.this);

                    } catch (Exception e) {
                        Log.e("SendMail", e.getMessage(), e);
                        Toast.makeText(this, "Error sending Requested Radio.", Toast.LENGTH_SHORT).show();

                    }
                }

                break;

            case R.id.showGenreImageView:
                PopupMenu genrePopUp = new PopupMenu(this, v);
                genrePopUp.setOnMenuItemClickListener(this);
                genrePopUp.inflate(R.menu.menu_genre);
                genrePopUp.show();

                break;

            case R.id.showTopicImageView:
                PopupMenu topicPopUp = new PopupMenu(this, v);
                topicPopUp.setOnMenuItemClickListener(this);
                topicPopUp.inflate(R.menu.menu_topic);
                topicPopUp.show();

                break;

            case R.id.addImageImageView:
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                    if (checkSelfPermission(Manifest.permission.READ_EXTERNAL_STORAGE)
                            == PackageManager.PERMISSION_GRANTED) {
                        Intent intent = new Intent(Intent.ACTION_PICK);
                        intent.setType("image/*");
                        String[] mimeTypes = {"image/jpeg", "image/png"};
                        intent.putExtra(Intent.EXTRA_MIME_TYPES, mimeTypes);
                        startActivityForResult(intent, TransistorKeys.GALLERY);
                    } else {

                        ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.READ_EXTERNAL_STORAGE}, 1);
                    }
                } else { //permission is automatically granted on sdk<23 upon installation
                    Intent intent = new Intent(Intent.ACTION_PICK);
                    intent.setType("image/*");
                    String[] mimeTypes = {"image/jpeg", "image/png"};
                    intent.putExtra(Intent.EXTRA_MIME_TYPES, mimeTypes);
                    startActivityForResult(intent, TransistorKeys.GALLERY);
                }

                break;
        }

    }


    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {

        if (resultCode != RESULT_OK) {
            return;
        }
        if (requestCode == TransistorKeys.GALLERY) {
            final Bundle extras = data.getExtras();
            if (extras != null) {
                //Get image
//                Bitmap attachedImageBitmap = extras.getParcelable("data");
//                addImageImageView.setImageBitmap(attachedImageBitmap);

                Uri selectedImage = data.getData();

                picturePath = getPath(RequestRadioActivity.this.getApplicationContext(), selectedImage);

                addImageImageView.setImageURI(selectedImage);
                logoET.setText(picturePath);
                logoET.setVisibility(View.GONE);
                imageSelected = true;


            }
        }
    }

    public static String getPath(Context context, Uri uri) {
        String result = null;
        String[] proj = {MediaStore.Images.Media.DATA};
        Cursor cursor = context.getContentResolver().query(uri, proj, null, null, null);
        if (cursor != null) {
            if (cursor.moveToFirst()) {
                int column_index = cursor.getColumnIndexOrThrow(proj[0]);
                result = cursor.getString(column_index);
            }
            cursor.close();
        }
        if (result == null) {
            result = "Not found";
        }
        return result;
    }

    @Override
    public boolean onMenuItemClick(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.genrePop:
                genreET.setText(item.getTitle());
                return true;
            case R.id.genreClassic:
                genreET.setText(item.getTitle());
                return true;
            case R.id.genreRock:
                genreET.setText(item.getTitle());
                return true;
            case R.id.genreTalk:
                genreET.setText(item.getTitle());
                return true;
            case R.id.genreReligion:
                genreET.setText(item.getTitle());
                return true;
            case R.id.genreNews:
                genreET.setText(item.getTitle());
                return true;
            case R.id.genreSports:
                genreET.setText(item.getTitle());
                return true;
            case R.id.genreOther:
                genreET.setText(item.getTitle());
                return true;

            case R.id.topicPop:
                topicET.setText(item.getTitle());
                return true;
            case R.id.topicClassic:
                topicET.setText(item.getTitle());
                return true;
            case R.id.topicRock:
                topicET.setText(item.getTitle());
                return true;
            case R.id.topicTalk:
                topicET.setText(item.getTitle());
                return true;
            case R.id.topicReligion:
                topicET.setText(item.getTitle());
                return true;
            case R.id.topicNews:
                topicET.setText(item.getTitle());
                return true;
            case R.id.topicSports:
                topicET.setText(item.getTitle());
                return true;
            case R.id.topicOther:
                topicET.setText(item.getTitle());
                return true;
            default:
                return false;
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                onBackPressed();
                return true;

            default:
                return super.onOptionsItemSelected(item);
        }

    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
    }

    public class ViewDialog {

        public void showDialog(Activity activity) {
            Dialog dialog = new Dialog(activity);
            dialog.setCanceledOnTouchOutside(true);

            dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
            dialog.setCancelable(true);
            dialog.setContentView(R.layout.dialog_submitted);
            dialog.getWindow().setBackgroundDrawableResource(android.R.color.transparent);

            dialog.setOnCancelListener(new DialogInterface.OnCancelListener() {
                @Override
                public void onCancel(DialogInterface dialog) {
                    Handler handler = new Handler();
                    handler.postDelayed(new Runnable() {
                        public void run() {

                            RequestRadioActivity.this.finish();
                            onBackPressed();
                        }
                    }, 100);

                }
            });


            WindowManager.LayoutParams lp = dialog.getWindow().getAttributes();
            lp.dimAmount=0.8f;
            dialog.getWindow().setAttributes(lp);
            dialog.getWindow().addFlags(WindowManager.LayoutParams.FLAG_DIM_BEHIND);
            dialog.getWindow().getAttributes().windowAnimations = R.style.DialogAnimation;

            dialog.show();

            if(dialog.isShowing()){
                Handler handler = new Handler();
                handler.postDelayed(new Runnable() {
                    public void run() {

                        RequestRadioActivity.this.finish();
                        onBackPressed();
                    }
                }, 5000);
            }


        }
    }


}

